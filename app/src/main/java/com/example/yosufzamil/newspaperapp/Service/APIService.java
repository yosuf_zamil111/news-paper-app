package com.example.yosufzamil.newspaperapp.Service;

import com.example.yosufzamil.newspaperapp.Model.HomeModel;
import com.example.yosufzamil.newspaperapp.Model.MostVisitedModel;
import com.example.yosufzamil.newspaperapp.Model.TopStoriesModel;
import com.example.yosufzamil.newspaperapp.Model.User;

import java.util.List;

import retrofit2.Call;
import retrofit2.http.GET;

/**
 * Created by Yosuf Zamil on 18-Apr-17.
 */
public interface APIService {

    @GET("json_bangla")
    Call<List<User>> getUserData();

    @GET("feeds/flowers.json")
    Call<List<MostVisitedModel>> getThemeData();

    @GET("api/home/10/0")
    Call<List<HomeModel.News>> getHomeNews();

}
