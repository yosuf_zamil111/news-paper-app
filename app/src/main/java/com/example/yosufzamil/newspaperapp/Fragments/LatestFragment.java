package com.example.yosufzamil.newspaperapp.Fragments;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.example.yosufzamil.newspaperapp.Adapter.UserAdapter;
import com.example.yosufzamil.newspaperapp.Model.User;
import com.example.yosufzamil.newspaperapp.Network.ApiClient;
import com.example.yosufzamil.newspaperapp.R;
import com.example.yosufzamil.newspaperapp.Service.APIService;
import java.util.List;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class LatestFragment extends Fragment {

    private LinearLayoutManager linearLayoutManager;
    RecyclerView recyclerView;

    public LatestFragment() {

    }

    public static LatestFragment newInstance(int param1) {
        LatestFragment fragment = new LatestFragment();
        return fragment;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        View view = inflater.inflate(R.layout.fragment_latest, container, false);

        prepareStage(view);

        return view;
    }

    void prepareStage(View view){

        recyclerView = (RecyclerView) view.findViewById(R.id.recycler_view_latest);

        try {

            APIService service = ApiClient.getRetrofit().create(APIService.class);
            Call<List<User>> call = service.getUserData();

            call.enqueue(new Callback<List<User>>() {
                @Override
                public void onResponse(Call<List<User>> call, Response<List<User>> response) {

                    List<User> userList = response.body();

                    linearLayoutManager = new LinearLayoutManager(getActivity().getApplication());
                    recyclerView.setLayoutManager(linearLayoutManager);

                    UserAdapter userAdapter = new UserAdapter(userList);

                    recyclerView.setAdapter(userAdapter);

                }

                @Override
                public void onFailure(Call<List<User>> call, Throwable t) {

                }
            });


        }catch (Exception e){

        }

    }

}
