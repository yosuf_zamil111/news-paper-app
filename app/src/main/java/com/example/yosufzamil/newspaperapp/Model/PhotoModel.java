package com.example.yosufzamil.newspaperapp.Model;

/**
 * Created by Yosuf Zamil on 22-Apr-17.
 */

public class PhotoModel {

    public String imageOne, imageTwo;

    public String getImageOne() {
        return imageOne;
    }

    public void setImageOne(String imageOne) {
        this.imageOne = imageOne;
    }

    public String getImageTwo() {
        return imageTwo;
    }

    public void setImageTwo(String imageTwo) {
        this.imageTwo = imageTwo;
    }
}