package com.example.yosufzamil.newspaperapp.Adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.yosufzamil.newspaperapp.Model.HomeModel;
import com.example.yosufzamil.newspaperapp.Model.User;
import com.example.yosufzamil.newspaperapp.R;
import com.squareup.picasso.Picasso;

import java.util.List;

/**
 * Created by Yosuf Zamil on 23-Apr-17.
 */
public class HomeAdapter extends RecyclerView.Adapter<HomeAdapter.ViewHolder> {

    Context ctx;
    List<HomeModel.News> item;

    public class ViewHolder extends RecyclerView.ViewHolder{

        public ImageView oneImg;

        public ViewHolder(View v) {
            super(v);

            oneImg = (ImageView) v.findViewById(R.id.homeImageOne);

            ctx = v.getContext();
        }

    }

    public HomeAdapter(List<HomeModel.News> list){
        this.item = list;
    }



    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.home_adapter, parent, false);
        HomeAdapter.ViewHolder vh = new HomeAdapter.ViewHolder(v);
        return vh;
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {

        Picasso.with(ctx)
                .load(item.get(position).getImageLarge())
                .placeholder(R.drawable.phone)
                //.error(R.drawable.transparent)
                .fit()
                .into(holder.oneImg);

    }

    @Override
    public int getItemCount() {
        return item.size();
    }

}
