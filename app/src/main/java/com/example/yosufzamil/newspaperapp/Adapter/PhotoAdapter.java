package com.example.yosufzamil.newspaperapp.Adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import com.example.yosufzamil.newspaperapp.Model.PhotoModel;
import com.example.yosufzamil.newspaperapp.R;
import com.squareup.picasso.Picasso;

import java.util.List;

/**
 * Created by Yosuf Zamil on 22-Apr-17.
 */
public class PhotoAdapter extends RecyclerView.Adapter<PhotoAdapter.ViewHolder> {

    Context ctx;
    List<PhotoModel> photoModels;

    public class ViewHolder extends RecyclerView.ViewHolder {

        public ImageView imageOne,imageTwo;

        public ViewHolder(View view) {
            super(view);

            imageOne = (ImageView) view.findViewById(R.id.imageOne);
            imageTwo = (ImageView) view.findViewById(R.id.imageTwo);

            ctx = view.getContext();
        }
    }

    public PhotoAdapter(List<PhotoModel> photoModels) {
        this.photoModels = photoModels;
    }


    @Override
    public PhotoAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.photo_adapter, parent, false);
        PhotoAdapter.ViewHolder vh = new PhotoAdapter.ViewHolder(v);
        return vh;
    }

    @Override
    public void onBindViewHolder(PhotoAdapter.ViewHolder holder, int position) {


        Picasso.with(ctx)
                .load(photoModels.get(position).getImageOne())
                .placeholder(R.drawable.phone)
                //.error(R.drawable.transparent)
                .fit()
                .into(holder.imageOne);

        Picasso.with(ctx)
                .load(photoModels.get(position).getImageTwo())
                .placeholder(R.drawable.phone)
                //.error(R.drawable.transparent)
                .fit()
                .into(holder.imageTwo);


    }

    @Override
    public int getItemCount() {
        return photoModels.size();
    }
}