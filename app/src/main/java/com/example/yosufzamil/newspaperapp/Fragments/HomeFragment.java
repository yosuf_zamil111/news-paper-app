package com.example.yosufzamil.newspaperapp.Fragments;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.example.yosufzamil.newspaperapp.Adapter.HomeAdapter;
import com.example.yosufzamil.newspaperapp.Model.HomeModel;
import com.example.yosufzamil.newspaperapp.Network.ApiClient;
import com.example.yosufzamil.newspaperapp.R;
import com.example.yosufzamil.newspaperapp.Service.APIService;

import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class HomeFragment extends Fragment {

    private LinearLayoutManager linearLayoutManager;
    RecyclerView recyclerView;

    public HomeFragment() {
    }

    public static HomeFragment newInstance(int param1) {
        HomeFragment fragment = new HomeFragment();
        return fragment;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        View view = inflater.inflate(R.layout.fragment_home, container, false);

        prepareStage(view);

        return view;

    }

    void prepareStage(View view) {

        recyclerView = (RecyclerView) view.findViewById(R.id.recycler_view_home);

        Toast.makeText(getContext(), "ELSE", Toast.LENGTH_SHORT).show();

        try {

            APIService service = ApiClient.getRetrofit3().create(APIService.class);
            Call<List<HomeModel.News>> call = service.getHomeNews();

            call.enqueue(new Callback<List<HomeModel.News>>() {
                @Override
                public void onResponse(Call<List<HomeModel.News>> call, Response<List<HomeModel.News>> response) {

                    List<HomeModel.News> userList = response.body();

                    Toast.makeText(getContext(), "IF "+userList.get(0).getTitle(), Toast.LENGTH_SHORT).show();

                    linearLayoutManager = new LinearLayoutManager(getActivity().getApplication());
                    recyclerView.setLayoutManager(linearLayoutManager);

                    HomeAdapter homeAdapter = new HomeAdapter(userList);

                    recyclerView.setAdapter(homeAdapter);

                }

                @Override
                public void onFailure(Call<List<HomeModel.News>> call, Throwable t) {

                }
            });



        }catch (Exception e){

        }

    }

}
