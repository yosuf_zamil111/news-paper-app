package com.example.yosufzamil.newspaperapp.Adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import com.example.yosufzamil.newspaperapp.Model.MostVisitedModel;
import com.example.yosufzamil.newspaperapp.R;
import java.util.List;

/**
 * Created by Yosuf Zamil on 18-Apr-17.
 */
public class MostVisitedAdapter extends RecyclerView.Adapter<MostVisitedAdapter.ViewHolder> {

    Context ctx;
    List<MostVisitedModel> mostVisitedModels;

    public class ViewHolder extends RecyclerView.ViewHolder {

        public TextView theme_name, arrange, created_time, last_modified;

        public ViewHolder(View v) {
            super(v);

            theme_name = (TextView) v.findViewById(R.id.theme_name);
            arrange = (TextView) v.findViewById(R.id.arrange);
            created_time = (TextView) v.findViewById(R.id.created_time);
            last_modified = (TextView) v.findViewById(R.id.last_modified);

            ctx = v.getContext();
        }
    }

    public MostVisitedAdapter(List<MostVisitedModel> mostVisitedModels) {
        this.mostVisitedModels = mostVisitedModels;
    }


    @Override
    public MostVisitedAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.most_visited_adaapter, parent, false);
        MostVisitedAdapter.ViewHolder vh = new MostVisitedAdapter.ViewHolder(v);
        return vh;
    }

    @Override
    public void onBindViewHolder(MostVisitedAdapter.ViewHolder holder, int position) {

        holder.theme_name.setText(mostVisitedModels.get(position).getCategory());
        holder.arrange.setText(mostVisitedModels.get(position).getPrice()+"");
        holder.created_time.setText(mostVisitedModels.get(position).getName());
        holder.last_modified.setText(mostVisitedModels.get(position).getProductId()+"");

    }

    @Override
    public int getItemCount() {
        return mostVisitedModels.size();
    }
}
