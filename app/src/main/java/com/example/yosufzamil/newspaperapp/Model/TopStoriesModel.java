package com.example.yosufzamil.newspaperapp.Model;

import android.widget.ImageView;

/**
 * Created by Yosuf Zamil on 18-Apr-17.
 */
public class TopStoriesModel {

    public String topStoriesHeading;
    public String topStoriesTime;


    public TopStoriesModel() {
    }

    public TopStoriesModel(String topStoriesHeading, String topStoriesTime) {
        this.topStoriesHeading = topStoriesHeading;
        this.topStoriesTime = topStoriesTime;
    }

    public String getTopStoriesHeading() {
        return topStoriesHeading;
    }

    public void setTopStoriesHeading(String topStoriesHeading) {
        this.topStoriesHeading = topStoriesHeading;
    }

    public String getTopStoriesTime() {
        return topStoriesTime;
    }

    public void setTopStoriesTime(String topStoriesTime) {
        this.topStoriesTime = topStoriesTime;
    }
}
