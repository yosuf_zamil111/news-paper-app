package com.example.yosufzamil.newspaperapp.Adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.yosufzamil.newspaperapp.Model.User;
import com.example.yosufzamil.newspaperapp.R;

import java.util.List;

/**
 * Created by Yosuf Zamil on 18-Apr-17.
 */
public class UserAdapter extends RecyclerView.Adapter<UserAdapter.ViewHolder>{

    Context ctx;
    List<User> item;

    public class ViewHolder extends RecyclerView.ViewHolder{

        public TextView name, hobby;

        public ViewHolder(View v) {
            super(v);

            name = (TextView) v.findViewById(R.id.name);
            hobby = (TextView) v.findViewById(R.id.hobby);

            ctx = v.getContext();
        }

    }

    public UserAdapter(List<User> list){
        this.item = list;
    }


    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.recycler_view_row, parent, false);
        UserAdapter.ViewHolder vh = new UserAdapter.ViewHolder(v);
        return vh;
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {

        holder.name.setText(item.get(position).getName());
        holder.hobby.setText(item.get(position).getHobby());

    }

    @Override
    public int getItemCount() {
        return item.size();
    }

}
