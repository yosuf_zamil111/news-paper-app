package com.example.yosufzamil.newspaperapp.UI;

import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.TabLayout;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;

import com.example.yosufzamil.newspaperapp.Fragments.HomeFragment;
import com.example.yosufzamil.newspaperapp.Fragments.LatestFragment;
import com.example.yosufzamil.newspaperapp.Fragments.MostVisitedFragment;
import com.example.yosufzamil.newspaperapp.Fragments.PhotoFragment;
import com.example.yosufzamil.newspaperapp.Fragments.TopStoriesFragment;
import com.example.yosufzamil.newspaperapp.R;
import com.example.yosufzamil.newspaperapp.ViewPagerAdapter;

public class MainActivity extends AppCompatActivity {

    Toolbar toolbar;
    TabLayout tabLayout;
    ViewPager viewPager;

    ViewPagerAdapter viewPagerAdapter;
    FloatingActionButton floatingActionButton;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        toolbar = (Toolbar) findViewById(R.id.toolBar);
        setSupportActionBar(toolbar);

        tabLayout = (TabLayout) findViewById(R.id.tabLayout);
        viewPager = (ViewPager) findViewById(R.id.viewPager);
        //floatingActionButton = (FloatingActionButton) findViewById(R.id.floatingActionButton);


        viewPagerAdapter = new ViewPagerAdapter(getSupportFragmentManager());

        viewPagerAdapter.addFragments(new TopStoriesFragment(), "Top Stories");
        viewPagerAdapter.addFragments(new LatestFragment(), "Latest");
        viewPagerAdapter.addFragments(new MostVisitedFragment(), "Most Visited");
        viewPagerAdapter.addFragments(new PhotoFragment(), "My Photo");
        viewPagerAdapter.addFragments(new HomeFragment(), "Home News");



        viewPager.setAdapter(viewPagerAdapter);
        //viewPager.setCurrentItem(1);

        tabLayout.setupWithViewPager(viewPager);

    }
}
