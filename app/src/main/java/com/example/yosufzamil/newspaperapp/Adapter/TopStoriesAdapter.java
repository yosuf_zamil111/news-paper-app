package com.example.yosufzamil.newspaperapp.Adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.yosufzamil.newspaperapp.Model.TopStoriesModel;
import com.example.yosufzamil.newspaperapp.R;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Yosuf Zamil on 18-Apr-17.
 */
public class TopStoriesAdapter extends RecyclerView.Adapter<TopStoriesAdapter.ViewHolder> {

    Context ctx;
    List<TopStoriesModel> topStoriesModels = new ArrayList<>();

    public class ViewHolder extends RecyclerView.ViewHolder {

        public ImageView top_stories_img;
        public TextView heading_news, hour_min_ago;

        public ViewHolder(View v) {
            super(v);

            top_stories_img = (ImageView) v.findViewById(R.id.top_stories_img);
            heading_news = (TextView) v.findViewById(R.id.heading_news);
            hour_min_ago = (TextView) v.findViewById(R.id.hour_min_ago);

            ctx = v.getContext();
        }
    }

    public TopStoriesAdapter(List<TopStoriesModel> topStoriesModels) {
        this.topStoriesModels = topStoriesModels;
    }

    @Override
    public TopStoriesAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.book_adapter, parent, false);
        TopStoriesAdapter.ViewHolder vh = new TopStoriesAdapter.ViewHolder(v);
        return vh;
    }

    @Override
    public void onBindViewHolder(TopStoriesAdapter.ViewHolder holder, int position) {

        holder.heading_news.setText(topStoriesModels.get(position).getTopStoriesHeading());
        holder.hour_min_ago.setText(topStoriesModels.get(position).getTopStoriesTime());

    }

    @Override
    public int getItemCount() {
        return topStoriesModels.size();
    }
}
