package com.example.yosufzamil.newspaperapp.Model;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by Yosuf Zamil on 18-Apr-17.
 */
public class User {

    private String name;
    private String hobby;
    private Map<String, Object> additionalProperties = new HashMap<String, Object>();

    public User() {
    }

    public User(String name, String hobby) {
        super();
        this.name = name;
        this.hobby = hobby;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getHobby() {
        return hobby;
    }

    public void setHobby(String hobby) {
        this.hobby = hobby;
    }

    public Map<String, Object> getAdditionalProperties() {
        return this.additionalProperties;
    }

    public void setAdditionalProperty(String name, Object value) {
        this.additionalProperties.put(name, value);
    }

}
