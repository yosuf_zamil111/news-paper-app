package com.example.yosufzamil.newspaperapp.Network;

import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

/**
 * Created by Yosuf Zamil on 18-Apr-17.
 */
public class ApiClient {

    public static final String BASE_URL = "http://shaoniiuc.com/";
    private static Retrofit retrofit = null;

    public static Retrofit getRetrofit(){
        if(retrofit == null){

            retrofit = new Retrofit.Builder()
                    .baseUrl(BASE_URL)
                    .addConverterFactory(GsonConverterFactory.create())
                    .build();
        }
        return retrofit;
    }

    public static final String BASE_URL2 = "http://services.hanselandpetal.com/";
    private static Retrofit retrofit2 = null;

    public static Retrofit getRetrofit2(){
        if(retrofit2 == null){

            retrofit2 = new Retrofit.Builder()
                    .baseUrl(BASE_URL2)
                    .addConverterFactory(GsonConverterFactory.create())
                    .build();
        }
        return retrofit2;
    }

    public static final String BASE_URL3 = "http://104.131.189.188/";
    private static Retrofit retrofit3 = null;

    public static Retrofit getRetrofit3(){
        if(retrofit3 == null){

            retrofit3 = new Retrofit.Builder()
                    .baseUrl(BASE_URL3)
                    .addConverterFactory(GsonConverterFactory.create())
                    .build();
        }
        return retrofit3;
    }

}
