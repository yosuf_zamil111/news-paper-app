package com.example.yosufzamil.newspaperapp.Fragments;


import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.example.yosufzamil.newspaperapp.Adapter.TopStoriesAdapter;
import com.example.yosufzamil.newspaperapp.Model.TopStoriesModel;
import com.example.yosufzamil.newspaperapp.R;

import java.util.ArrayList;
import java.util.List;

public class TopStoriesFragment extends Fragment {

    RecyclerView recycler_view_top_stories;
    TopStoriesAdapter topStoriesAdapter;
    List<TopStoriesModel> topStoriesModels = new ArrayList<TopStoriesModel>();;

    public TopStoriesFragment() {

    }

  /*  public static TopStoriesFragment newInstance(int param1) {
        TopStoriesFragment fragment = new TopStoriesFragment();
        return fragment;
    }*/

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        View view = inflater.inflate(R.layout.fragment_top_stories, container, false);

        preparStage(view);

        return view;
    }

    void preparStage(View view){

        recycler_view_top_stories = (RecyclerView) view.findViewById(R.id.recycler_view_top_stories);
        recycler_view_top_stories.setHasFixedSize(true);

        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(getContext());
        linearLayoutManager.setOrientation(LinearLayoutManager.VERTICAL);

        recycler_view_top_stories.setLayoutManager(linearLayoutManager);

        for(int i=0; i<20; i++) {
            TopStoriesModel topStoriesModel = new TopStoriesModel();
            topStoriesModel.setTopStoriesHeading("Heading Here Ashbe");
            topStoriesModel.setTopStoriesTime("Time and Hour ashbe");
            topStoriesModels.add(topStoriesModel);
        }

        topStoriesAdapter = new TopStoriesAdapter(topStoriesModels);
        recycler_view_top_stories.setAdapter(topStoriesAdapter);
    }

}
