package com.example.yosufzamil.newspaperapp.Fragments;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.example.yosufzamil.newspaperapp.Adapter.PhotoAdapter;
import com.example.yosufzamil.newspaperapp.Model.PhotoModel;
import com.example.yosufzamil.newspaperapp.R;

import java.util.ArrayList;
import java.util.List;

public class PhotoFragment extends Fragment {

    private LinearLayoutManager linearLayoutManager;
    RecyclerView recyclerView;

    public PhotoFragment() {

    }

    public static PhotoFragment newInstance(int param1) {
        PhotoFragment fragment = new PhotoFragment();
        return fragment;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_photo, container, false);

        preparStage(view);

        return view;
    }

    void preparStage(View view){

        recyclerView = (RecyclerView) view.findViewById(R.id.recycler_view_photo);

        List<PhotoModel> photoModels = new ArrayList<>();

        for(int i = 0; i < 20; i++) {
            PhotoModel lm = new PhotoModel();
            lm.setImageOne("http://i.stack.imgur.com/dCyzH.png");
            lm.setImageTwo("http://i.stack.imgur.com/dCyzH.png");
            photoModels.add(lm);
        }

        linearLayoutManager = new LinearLayoutManager(getActivity().getApplication());
        recyclerView.setLayoutManager(linearLayoutManager);
        PhotoAdapter photoAdapter = new PhotoAdapter(photoModels);
        recyclerView.setAdapter(photoAdapter);
    }

}
