package com.example.yosufzamil.newspaperapp.Fragments;

import android.app.ProgressDialog;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.example.yosufzamil.newspaperapp.Adapter.MostVisitedAdapter;
import com.example.yosufzamil.newspaperapp.Model.MostVisitedModel;
import com.example.yosufzamil.newspaperapp.Network.ApiClient;
import com.example.yosufzamil.newspaperapp.R;
import com.example.yosufzamil.newspaperapp.Service.APIService;

import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class MostVisitedFragment extends Fragment {

    private LinearLayoutManager linearLayoutManager;
    RecyclerView recyclerView;
    MostVisitedAdapter mostVisitedAdapter;
    List<MostVisitedModel> userList;

    public MostVisitedFragment() {

    }

    public static MostVisitedFragment newInstance(int param1) {
        MostVisitedFragment fragment = new MostVisitedFragment();
        return fragment;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_most_visited, container, false);

        prepareStage(view);

        return view;
    }

    void prepareStage(View view){

        recyclerView = (RecyclerView) view.findViewById(R.id.recycler_view_most_visited);

        try {
            APIService service = ApiClient.getRetrofit2().create(APIService.class);
            Call<List<MostVisitedModel>> call = service.getThemeData();

            call.enqueue(new Callback<List<MostVisitedModel>>() {
                @Override
                public void onResponse(Call<List<MostVisitedModel>> call, Response<List<MostVisitedModel>> response) {

                    new AsyncTaskRunner().execute();

                    userList = response.body();

                    mostVisitedAdapter = new MostVisitedAdapter(userList);

                    linearLayoutManager = new LinearLayoutManager(getContext());
                    recyclerView.setLayoutManager(linearLayoutManager);

                    recyclerView.setAdapter(mostVisitedAdapter);
                }

                @Override
                public void onFailure(Call<List<MostVisitedModel>> call, Throwable t) {

                }
            });

        }catch (Exception e){

        }

    }

    private class AsyncTaskRunner extends AsyncTask<Void, Void, Void> {

        ProgressDialog progressDialog;

        @Override
        protected Void doInBackground(Void... params) {

            return null;
        }

        @Override
        protected void onPostExecute(Void result) {
            // execution of result of Long time consuming operation

            progressDialog.dismiss();

        }

        @Override
        protected void onPreExecute() {
            progressDialog = ProgressDialog.show(getContext(), "Progress Running", "Please Wait for a while.");
        }

        @Override
        protected void onProgressUpdate(Void... text) {

        }
    }

}
